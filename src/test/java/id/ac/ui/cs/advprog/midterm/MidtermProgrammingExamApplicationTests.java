package id.ac.ui.cs.advprog.midterm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
class MidtermProgrammingExamApplicationTests {


	@Test
	public void testMainMethodRuns() {
		MidtermProgrammingExamApplication.main(new String[] {});
		assertTrue(true, "Test main method");
	}
}
