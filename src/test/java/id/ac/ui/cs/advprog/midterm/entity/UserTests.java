package id.ac.ui.cs.advprog.midterm.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserTests {

    @Autowired
    private static User user;

    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(8080);
        user.setName("Riza");
        user.setEmail("rkusumaa@yahoo.com");
    }
    @Test
    void checkIfNameEqualsAndExists() {
        assertEquals("Riza", user.getName());
        assertNotNull(user.getName());
    }
    @Test
    void checkIfEmailEqualsAndExists() {
        assertEquals("rkusumaa@yahoo.com", user.getEmail());
        assertNotNull(user.getEmail());
    }

    @Test
    void checkIfIdEqualsAndExists() {
        assertEquals(8080, user.getId());
    }

    @Test
    void checkToStringMethod() {
        assertEquals("User{id=8080, name='Riza', email='rkusumaa@yahoo.com'}", user.toString());
    }
}
