package id.ac.ui.cs.advprog.midterm.controller;

import org.springframework.test.context.junit4.SpringRunner;

public @interface RunWith {

	Class<SpringRunner> value();

}
